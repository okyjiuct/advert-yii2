<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_083150_table_settings extends Migration
{
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'slug' => $this->string('128'),
            'title' => $this->string('512'),
            'proxy' => $this->text(),
            'proxy_login' => $this->string('128'),
            'metrika' => $this->integer(),
            'li' => $this->integer(),
            'analitycs' => $this->integer(),
            'yandex_verifitacion' => $this->string('128'),
            'google_verifitacion' => $this->string('128'),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'user_id' => $this->integer()->notNull(),
            'top' => $this->text(),
            'topMob' => $this->text(),
            'sidebar' => $this->text(),
            'bottom' => $this->text(),
            'bottomMob' => $this->text(),
            'context' => $this->text(),
            'clickunder' => $this->text(),
            'otherAd' => $this->text(),
        ]);

        $this->addForeignKey('settings_to_user', 'settings', 'user_id', 'users', 'id');
    }

    public function down()
    {
        echo "m151010_083150_table_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
