<?php

use yii\db\Schema;
use yii\db\Migration;

class m151022_185037_search_model extends Migration
{
    public function up()
    {
        $this->createTable('search', [
            'id' => $this->primaryKey(),
            'search' => $this->string('256')->notNull(),
            'region_id' => $this->integer()->defaultValue(0),
            'city_id' => $this->integer()->defaultValue(0),
            'category_id' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('search_to_city', 'search', 'city_id', 'regions', 'id');
        $this->addForeignKey('search_to_region', 'search', 'region_id', 'regions', 'id');
        $this->addForeignKey('search_to_category', 'search', 'category_id', 'categories', 'id');
    }

    public function down()
    {
        echo "m151022_185037_search_model cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
