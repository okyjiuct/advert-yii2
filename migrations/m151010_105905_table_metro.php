<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_105905_table_metro extends Migration
{
    public function up()
    {
        $this->createTable('metro', [
            'id' => $this->primaryKey(),
            'slug' => $this->string('128'),
            'name' => $this->string('128'),
        ]);

        $this->addForeignKey('metro_to_ads', 'ads', 'metro_id', 'metro', 'id');
    }

    public function down()
    {
        echo "m151010_105905_table_metro cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
