<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_093749_add_column_in_settings extends Migration
{
    public function up()
    {
        $this->addColumn('settings', 'query_to_search', $this->smallInteger(3)->defaultValue(50));
    }

    public function down()
    {
        echo "m151119_093749_add_column_in_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
