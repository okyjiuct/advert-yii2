<?php

use yii\db\Schema;
use yii\db\Migration;

class m151017_075535_add_slug_in_ads extends Migration
{
    public function up()
    {
        $this->addColumn('ads', 'slug', $this->string(128)->notNull());
    }

    public function down()
    {
        echo "m151017_075535_add_slug_in_ads cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
