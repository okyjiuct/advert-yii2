<?php

use yii\db\Schema;
use yii\db\Migration;

class m151120_084549_add_auto_query_in_search extends Migration
{
    public function up()
    {
        $this->addColumn('search', 'auto_query', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m151120_084549_add_auto_query_in_search cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
