<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_082306_table_category extends Migration
{
    public function up()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'slug' => $this->string('128'),
            'name' => $this->string('128'),
        ]);

        $this->addForeignKey('categories_to_ads', 'ads', 'category_id', 'categories', 'id');
    }

    public function down()
    {
        echo "m151010_082306_table_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
