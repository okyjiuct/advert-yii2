<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_112619_proxy extends Migration
{
    public function init()
    {
        $this->db = 'db2';
        parent::init();
    }

    public function up()
    {
        if ($this->db->schema->getTableSchema('proxy', true) === null) {
            $this->createTable('proxy', [
                'id' => $this->primaryKey(),
                'ip_port' => $this->string('32')->notNull(),
                'login_pass' => $this->string('32')->notNull(),
                'error' => $this->smallInteger('1')->notNull()->defaultValue(0),
                'used' => $this->smallInteger('1')->notNull()->defaultValue(0),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }
    }

    public function down()
    {
        echo "m151010_112619_proxy cannot be reverted.\n";

        return false;
    }


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }

}
