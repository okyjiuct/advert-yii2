<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_082055_table_ads extends Migration
{
    public function up()
    {
        $this->createTable('ads', [
            'id' => $this->primaryKey(),
            'title' => $this->string('512')->notNull(),
            'text' => $this->text(),
            'link' => $this->string('512')->notNull(),
            'image' => $this->string('512'),
            'price' => $this->string('512'),
            'category_id' => $this->integer(),
            'region_id' => $this->integer(),
            'city_id' => $this->integer(),
            'metro_id' => $this->integer(),
            'author' => $this->string('512'),
            'hash' => $this->string('32')->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->execute('ALTER TABLE `ads` ADD FULLTEXT(`title`,`text`)');
        $this->execute('ALTER TABLE `ads` ADD UNIQUE(`hash`)');
    }

    public function down()
    {
        echo "m151010_082055_table_ads cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
