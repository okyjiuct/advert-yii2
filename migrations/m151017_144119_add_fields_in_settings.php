<?php

use yii\db\Schema;
use yii\db\Migration;

class m151017_144119_add_fields_in_settings extends Migration
{
    public function up()
    {
        $this->dropColumn('settings', 'old_ads_days');

        $this->addColumn('settings', 'use_proxy', $this->smallInteger(1)->defaultValue(1));
    }

    public function down()
    {
        echo "m151017_144119_add_fields_in_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
