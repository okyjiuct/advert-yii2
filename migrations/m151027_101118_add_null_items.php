<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_101118_add_null_items extends Migration
{
    public function up()
    {
        $this->execute("SET sql_mode='NO_AUTO_VALUE_ON_ZERO'; INSERT INTO `regions` (`id`, `slug`, `name`) VALUES (0, 0, 0)");
        $this->execute("SET sql_mode='NO_AUTO_VALUE_ON_ZERO'; INSERT INTO `cities` (`id`, `slug`, `name`, `region_id`) VALUES (0, 0, 0, 0)");
        $this->execute("SET sql_mode='NO_AUTO_VALUE_ON_ZERO'; INSERT INTO `categories` (`id`, `slug`, `name`) VALUES (0, 0, 0)");
    }

    public function down()
    {
        echo "m151027_101118_add_null_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
