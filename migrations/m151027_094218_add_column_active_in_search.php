<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_094218_add_column_active_in_search extends Migration
{
    public function up()
    {
        $this->addColumn('search', 'active', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m151027_094218_add_column_active_in_search cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
