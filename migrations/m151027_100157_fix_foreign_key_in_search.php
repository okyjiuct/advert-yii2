<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_100157_fix_foreign_key_in_search extends Migration
{
    public function up()
    {
        $this->dropForeignKey('search_to_city', 'search');
        $this->addForeignKey('search_to_city', 'search', 'city_id', 'cities', 'id');
    }

    public function down()
    {
        echo "m151027_100157_fix_foreign_key_in_search cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
