<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_105835_table_regions extends Migration
{
    public function up()
    {
        $this->createTable('regions', [
            'id' => $this->primaryKey(),
            'slug' => $this->string('128'),
            'name' => $this->string('128'),
            'used' => $this->smallInteger()->defaultValue(0),
        ]);

        $this->addForeignKey('regions_to_ads', 'ads', 'region_id', 'regions', 'id');
    }

    public function down()
    {
        echo "m151010_105835_table_regions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
