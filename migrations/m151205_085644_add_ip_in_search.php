<?php

use yii\db\Schema;
use yii\db\Migration;

class m151205_085644_add_ip_in_search extends Migration
{
    public function up()
    {
        $this->addColumn('search', 'ip', $this->string(15));
    }

    public function down()
    {
        echo "m151205_085644_add_ip_in_search cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
