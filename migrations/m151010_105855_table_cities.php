<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_105855_table_cities extends Migration
{
    public function up()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'slug' => $this->string('128'),
            'name' => $this->string('128'),
        ]);

        $this->addForeignKey('cities_to_ads', 'ads', 'city_id', 'cities', 'id');
    }

    public function down()
    {
        echo "m151010_105855_table_cities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
