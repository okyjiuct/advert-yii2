<?php

use yii\db\Schema;
use yii\db\Migration;

class m151018_072435_region_id_in_cities extends Migration
{
    public function up()
    {
        $this->addColumn('cities', 'region_id', $this->integer());
        $this->addForeignKey('regions_to_cities', 'cities', 'region_id', 'regions', 'id');
    }

    public function down()
    {
        echo "m151018_072435_region_id_in_cities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
