<?php

use yii\db\Schema;
use yii\db\Migration;

class m151011_101736_fix_name_column_in_settings extends Migration
{
    public function up()
    {
        $this->dropColumn('settings', 'yandex_verifitacion');
        $this->dropColumn('settings', 'google_verifitacion');

        $this->addColumn('settings', 'yandex_verification', $this->string(128));
        $this->addColumn('settings', 'google_verification', $this->string(128));
    }

    public function down()
    {
        echo "m151011_101736_fix_name_column_in_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
