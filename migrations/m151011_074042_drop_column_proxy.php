<?php

use yii\db\Schema;
use yii\db\Migration;

class m151011_074042_drop_column_proxy extends Migration
{
    public function up()
    {
        $this->dropColumn('settings', 'proxy');
        $this->dropColumn('settings', 'proxy_login');
    }

    public function down()
    {
        echo "m151011_074042_drop_column_proxy cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
