<?php

use yii\db\Schema;
use yii\db\Migration;

class m151011_101348_add_column_into_settings extends Migration
{
    public function up()
    {
        $this->addColumn('settings', 'index_navigation', $this->smallInteger(1)->defaultValue(1));
        $this->addColumn('settings', 'delete_old_ads', $this->smallInteger(1)->defaultValue(0));
        $this->addColumn('settings', 'old_ads_days', $this->smallInteger(3)->defaultValue(30));
    }

    public function down()
    {
        echo "m151011_101348_add_column_into_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
