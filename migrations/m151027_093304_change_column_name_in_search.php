<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_093304_change_column_name_in_search extends Migration
{
    public function up()
    {
        $this->dropColumn('search', 'search');
        $this->addColumn('search', 'query', $this->string(256));
    }

    public function down()
    {
        echo "m151027_093304_change_column_name_in_search cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
