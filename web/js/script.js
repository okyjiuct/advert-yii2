/**
 * Created by kohone on 20.10.2015.
 */

$.fn.ready(function () {

    // rand
    function getRandomArbitary(min, max)
    {
        var result = Math.random() * (max - min) + min;
        return result.toFixed(2);
    }

    // Spoiler
    $(document).on('click', '.spoiler-btn', function (e) {
        e.preventDefault()
        $(this).parent().children('.spoiler-body').collapse('toggle')
    });


    $(".to").on("click", function () {
        var nubmer = $(this).data('nubmer');

        var w = screen.width;
        var h = screen.height;
        var result = window.open('/to/' + nubmer, "", "width=" + w * getRandomArbitary(0.75, 0.9) + ",height=" + h * getRandomArbitary(0.6, 0.75) + ",resizable=yes,scrollbars=yes,status=yes");
        result.focus();
    });

    $( "#search_region_id" ).change(function() {
        var region_id = $( this ).val() || 0;

        $.ajax({
            url: '/site/view',
            data: 'id=' + region_id,
            type: 'get',
            async: true,
            success: function(data) {
                $('.field_search_city_id').empty().html(data);
            }
        });


    });

    $('#search').submit(function() {
        var query = $('#search_query').val() || 0;
        window.location = $(this).attr("action") + '/' + $('#search_region_id').val() + '/' + $('#search_city_id').val() + '/' + $('#search_category_id').val() + '?query=' + query;
        return false;
    });

});