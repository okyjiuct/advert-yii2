<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\widgets\SettingsWidget;
use app\components\widgets\SeoWidget;


use app\models\Search;
use app\components\Y;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= $this->title ?></title>
        <?php $this->head() ?>
        <?= SettingsWidget::widget(['options' => 'verifications']) ?>

        <?= SeoWidget::widget() ?>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "WebSite",
          "url": "<?= Y::domain(); ?>",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "<?= Y::domain(); ?>/search/0/0/0?query={search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }
        </script>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Главная',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'encodeLabels' => false,
                'items' => [
                    !Yii::$app->user->isGuest ?
                        ['label' => 'Настройки', 'url' => ['/settings/index']] : '',
                    !Yii::$app->user->isGuest ?
                        ['label' => 'Поисковые запросы <span class="label label-warning">' . Search::countSearch() . '</span>', 'url' => ['/search/index']] : '',
                    !Yii::$app->user->isGuest ?
                        ['label' => 'Прокси', 'url' => ['/proxy/index']] : '',
                    Yii::$app->user->isGuest ? '' :
                        [
                            'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']
                        ],
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; <?= date('Y') ?></p>
                <p class="pull-right"><?= Html::a('Полный список поисковых запросов', ['/site/queries']); ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
        <?= SettingsWidget::widget(['options' => 'clickunder']) ?>
        <?= SettingsWidget::widget(['options' => 'otherAd']) ?>
        <?= SettingsWidget::widget(['options' => 'counters']) ?>
    </body>
</html>
<?php $this->endPage() ?>
