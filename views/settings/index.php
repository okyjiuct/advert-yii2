<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Settings'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'slug',
            'title',
            'metrika',
            'li',
            // 'analitycs',
            // 'created_at',
            // 'updated_at',
            // 'user_id',
            // 'top:ntext',
            // 'topMob:ntext',
            // 'sidebar:ntext',
            // 'bottom:ntext',
            // 'bottomMob:ntext',
            // 'context:ntext',
            // 'clickunder:ntext',
            // 'otherAd:ntext',
            // 'index_navigation',
            // 'delete_old_ads',
            // 'old_ads_days',
            // 'yandex_verification',
            // 'google_verification',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
