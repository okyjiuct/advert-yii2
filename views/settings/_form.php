<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'use_proxy')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'metrika')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'yandex_verification')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'index_navigation')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'analytics')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'google_verification')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'delete_old_ads')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'li')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        </div><div class="col-md-4">
            <?= $form->field($model, 'query_to_search')->textInput() ?>
        </div>


        <div class="clearfix"></div>


        <div class="col-md-6">
            <?= $form->field($model, 'top')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'topMob')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'bottom')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'bottomMob')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'context')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'sidebar')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'clickunder')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'otherAd')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
