<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'slug',
            'title',
            'metrika',
            'li',
            'analitycs',
            'created_at',
            'updated_at',
            'user_id',
            'top:ntext',
            'topMob:ntext',
            'sidebar:ntext',
            'bottom:ntext',
            'bottomMob:ntext',
            'context:ntext',
            'clickunder:ntext',
            'otherAd:ntext',
            'index_navigation',
            'delete_old_ads',
            'old_ads_days',
            'yandex_verification',
            'google_verification',
        ],
    ]) ?>

</div>
