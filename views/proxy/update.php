<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Proxy */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Proxy',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proxies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ip_port, 'url' => ['view', 'id' => $model->id]];

?>
<div class="proxy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
