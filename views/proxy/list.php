<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 17.10.2015
 * Time: 10:02
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create Proxy List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proxies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_list', [
        'model' => $model,
        'count' => $count
    ]) ?>

</div>
