<?php

/* @var $this yii\web\View */


use yii\widgets\ListView;
use yii\helpers\Url;

use app\components\widgets\ListWidget;
use app\components\widgets\SearchWidget;
use app\models\Settings;
use app\components\Y;

$this->title = $title;

$h1 = '';
if (Y::thisAction() == 'search' && $dataProvider->getCount() > 0) {
    $h1 = '<h1 class="search text-center">' . $title . '</h1>';
    $url = Y::parseUrl();

    if ($url['query'] == 0) {
        $h1 = str_replace('по запросу 0 ', '', $h1);
    }
}

?>


<div class="site-index">
    <div class="row">
        <div class="col-md-12">

            <?= $h1; ?>

            <div class="spoiler">
                <?= ListWidget::widget(['options' => 'regions']) ?>
            </div>
            <div class="spoiler">
                <?= ListWidget::widget(['options' => 'cities']) ?>
            </div>
        </div>

        <div class="col-md-3">
            <?= SearchWidget::widget(); ?>
            <?= ListWidget::widget(['options' => 'randQueries']) ?>
        </div>

        <div class="col-md-9" itemtype="http://schema.org/ItemList">
            <link itemprop="url" href="<?= Url::to('@web', true); ?>"/>
            <?php

            $settings = Settings::getSettings();

            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_list',
                'summary' => '',
            ]);
            ?>
        </div>
    </div>
</div>
