<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 15.12.2015
 * Time: 16:04
 */

use yii\widgets\ListView;

$this->title = $title;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_queriesList'
]);
