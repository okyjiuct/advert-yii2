<?php

/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 18.10.2015
 * Time: 20:46
 */

use yii\helpers\Html;
use app\components\Y;

?>
<div class="col-md-12 ablock mbottom" itemscope itemtype="http://schema.org/Product">
    <div class="row">
        <div class="col-md-3 text-center mtop">
            <img itemprop="image" src="<?= $model->image; ?>"/>

            <div class="mtop" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <span itemprop="priceCurrency" content="RUB"></span>
                <h4 itemprop="price"><?= $model->price; ?></h4>
            </div>
        </div>
        <div class="col-md-9">
            <h2 class="to" data-nubmer="<?= Y::enCrypt($model->id . '###' . md5($model->title . microtime())); ?>" itemprop="name">
                <?=
                $model->title;
                ?>
            </h2>

            <div class="row">
                <div class="col-md-4">
                    <span
                        class="label label-info">
                            <?= Html::a(
                                $model->region->name ? '<i class="fa fa-map-marker"></i> ' . $model->region->name : '',
                                [
                                    'site/view',
                                    'region_id' => $model->region_id,
                                    'region' => $model->region->slug,
                                    'city_id' => 0,
                                    'city' => 0
                                ]);
                            ?>
                        </span>
                </div>
                <div class="col-md-4">
                        <span
                            class="label label-info">
                            <?= Html::a(
                                $model->city->name ? '<i class="fa fa-map-marker"></i> ' . $model->city->name : '',
                                [
                                    'site/view',
                                    'region_id' => $model->region_id,
                                    'region' => $model->region->slug,
                                    'city_id' => $model->city_id,
                                    'city' => $model->city->slug
                                ]);
                            ?>
                        </span>
                </div>
                <div class="col-md-4">
                    <div><span
                            class="label label-warning"><?= $model->category->name ? '<i class="fa fa-bars"></i> ' . $model->category->name : ''; ?></span>
                    </div>
                </div>
            </div>

            <div class="mtop" itemprop="description">
                <?= Y::cut($model->text, 400); ?>
            </div>

        </div>
    </div>

    <div class="row mtop">
        <div class="col-md-4 hidden-xs mtop">
            <div><?= $model->author ? '<i class="fa fa-user"></i> ' . $model->author : ''; ?></div>
        </div>
        <div class="col-md-4 col-xs-6 mtop">
            <?= Y::dateDiff($model->created_at); ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="btn btn-success pull-right to" data-nubmer="<?= Y::enCrypt($model->id . '###' . md5($model->title . microtime())); ?>"><i
                    class="fa fa-hand-pointer-o"></i> <?= Yii::t('app', 'Read more'); ?></div>

        </div>
    </div>
</div>