<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 17.10.2015
 * Time: 10:04
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Proxy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proxy-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'query_list')->textarea(['rows' => 6]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if($count > 0) echo '<button class="btn btn-warning">Добавлено ' . $count . ' запросов</button>';?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
