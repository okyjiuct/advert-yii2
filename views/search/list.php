<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 17.10.2015
 * Time: 10:02
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create Search List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Search'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proxy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_list', [
        'model' => $model,
        'count' => $count
    ]) ?>

</div>
