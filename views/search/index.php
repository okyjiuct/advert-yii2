<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Searches');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Search List'), ['list'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'query',
            [
                'label' => 'Авто-запрос',
                'value' => function ($model) {
                    return $model->auto_query ? 'Да' : 'Нет';
                },
            ],
            [
                'label' => 'Модерация',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->active) {
                        return Html::a('<i class="fa fa-close"></i> ' . Html::encode(Yii::t('app', 'Declined')), Url::to(['declined', 'id' => $model->id]), ['class' => 'btn btn-danger']);
                    } else {
                        return Html::a('<i class="fa fa-check"></i> ' . Html::encode(Yii::t('app', 'Approved')), Url::to(['approved', 'id' => $model->id]), ['class' => 'btn btn-success']);
                    }
                },
            ],
            [
                'label' => 'Регион',
                'value' => function ($model) {
                    return isset($model->region->name) ? $model->region->name : 'Не выбрано';
                },
            ],
            [
                'label' => 'Город',
                'value' => function ($model) {
                    return isset($model->city->name) ? $model->city->name : 'Не выбрано';
                },
            ],
            [
                'label' => 'Категория',
                'value' => function ($model) {
                    return isset($model->category->name) ? $model->category->name : 'Не выбрано';
                },
            ],
            [
                'label' => 'Добавлен',
                'value' => function ($model) {
                    return date("d.m.Y, H:i", $model->created_at);
                },
            ],

            'ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
