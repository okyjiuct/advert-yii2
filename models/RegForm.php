<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 09.08.2015
 * Time: 19:06
 */

namespace app\models;

use yii\base\Model;
use app\models\Settings;
use Yii;

class RegForm extends Model
{
    public $username;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password'], 'required'],
            ['username', 'string', 'min' => 5, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['username', 'unique',
                'targetClass' => Users::className(),
                'message' => 'Это имя уже занято.'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => Users::className(),
                'message' => 'Эта почта уже занята.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Электронная почта',
            'password' => 'Пароль'
        ];
    }

    public function reg()
    {
        $user = new Users();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status = Users::STATUS_USER;

        if ($user->save()) {
            $settings = new Settings;
            $settings->user_id = $user->id;
            $settings->save();

            return $user;
        }

        return null;
    }


}