<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\Y;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property integer $metrika
 * @property integer $li
 * @property integer $analitycs
 * @property string $yandex_verification
 * @property string $google_verification
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property string $top
 * @property string $topMob
 * @property string $sidebar
 * @property string $bottom
 * @property string $bottomMob
 * @property string $context
 * @property string $clickunder
 * @property string $otherAd
 * @property integer $index_navigation
 * @property integer $delete_old_ads
 * @property integer $old_ads_days
 *
 * @property Users $user
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metrika', 'li', 'analytics', 'created_at', 'updated_at', 'user_id', 'index_navigation', 'query_to_search', 'delete_old_ads', 'use_proxy'], 'integer'],
            [['user_id'], 'required'],
            [['top', 'topMob', 'sidebar', 'bottom', 'bottomMob', 'context', 'clickunder', 'otherAd'], 'string'],
            [['slug', 'yandex_verification', 'google_verification'], 'string', 'max' => 128],
            [['title'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'title' => Yii::t('app', 'Title'),
            'metrika' => Yii::t('app', 'Metrika'),
            'li' => Yii::t('app', 'Li'),
            'analytics' => Yii::t('app', 'Analytics'),
            'yandex_verification' => Yii::t('app', 'Yandex Verification'),
            'google_verification' => Yii::t('app', 'Google Verification'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'user_id' => Yii::t('app', 'User ID'),
            'top' => Yii::t('app', 'Top'),
            'topMob' => Yii::t('app', 'Top Mob'),
            'sidebar' => Yii::t('app', 'Sidebar'),
            'bottom' => Yii::t('app', 'Bottom'),
            'bottomMob' => Yii::t('app', 'Bottom Mob'),
            'context' => Yii::t('app', 'Context'),
            'clickunder' => Yii::t('app', 'Clickunder'),
            'query_to_search' => Yii::t('app', 'Query To Search'),
            'otherAd' => Yii::t('app', 'Other Ad'),
            'index_navigation' => Yii::t('app', 'Index Navigation'),
            'delete_old_ads' => Yii::t('app', 'Delete Old Ads'),
            'use_proxy' => Yii::t('app', 'Use Proxy')
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }


    public static function getSettings()
    {
        $key = 'settings';

        $data = Y::getCache($key);
        if ($data === false) {
            $data = Settings::find()->where(['>', 'user_id', 0])->one();
            Y::setCache($key, $data);
        }

        return $data;
    }


    public function afterSave($insert, $changedAttributes)
    {
        Y::flushCache();
    }
}
