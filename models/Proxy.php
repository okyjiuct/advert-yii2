<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\Y;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $id
 * @property string $ip_port
 * @property string $login_pass
 * @property integer $error
 * @property integer $used
 * @property integer $created_at
 * @property integer $updated_at
 */
class Proxy extends \yii\db\ActiveRecord
{
    public $ip_port_list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_port', 'login_pass'], 'required'],
            [['ip_port_list'], 'required', 'on' => 'list'],
            [['ip_port', 'login_pass'], 'trim'],
            [['error', 'used', 'created_at', 'updated_at'], 'integer'],
            ['ip_port', 'unique',
                'targetClass' => Proxy::className(),
                'message' => 'Такой прокси уже существует.'],
            [['ip_port', 'login_pass'], 'string', 'max' => 32]
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ip_port' => Yii::t('app', 'Ip Port'),
            'ip_port_list' => Yii::t('app', 'Ip Port List'),
            'login_pass' => Yii::t('app', 'Login Pass'),
            'error' => Yii::t('app', 'Error'),
            'used' => Yii::t('app', 'Used'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @return mixed
     * Получаем готовую для работы прокси
     */
    public static function getProxy()
    {
        Proxy::proxyNotError();

        $proxy = Proxy::find()->where(['error' => 0, 'used' => 0])->one();

        if ($proxy != null) {
            Proxy::setUsed($proxy);
        } else {
            // обнуляем все прокси
            Proxy::setUsed($proxy);

            $proxy = Proxy::getProxy();
            Proxy::setUsed($proxy);
        }

        return $proxy;
    }


    /**
     * @param $proxy
     * Ставим флажок "Прокси использован"
     */
    private static function setUsed($proxy)
    {
        if ($proxy != null) {
            $proxy->used = 1;
            $proxy->update();
        } else {
            Proxy::updateAll(['used' => 0]);
        }
    }

    /**
     * @param $proxy
     * Ставим флажок "Прокси c ошибкой"
     */
    public static function setError($proxy)
    {
        if ($proxy != null) {
            $proxy->error = 1;
            $proxy->update();
        }
    }


    /**
     * Возвращаем ошибочные прокси в работу
     */
    public static function proxyNotError()
    {
        $date = time() - 12 * 60 * 60;
        Proxy::updateAll(['error' => 0], ['<', 'updated_at', $date]);
    }


    public static function addList($list)
    {
        $ip_port_list = explode("\n", $list['Proxy']['ip_port_list']);
        $login_pass = $list['Proxy']['login_pass'];

        $i = 0;
        foreach ($ip_port_list as $item) {
            $proxy = new Proxy;
            $proxy->ip_port = $item;
            $proxy->login_pass = $login_pass;

            if ($proxy->save()) $i++;
        }

        return $i;
    }
}
