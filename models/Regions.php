<?php

namespace app\models;

use Yii;
use app\components\Y;

/**
 * This is the model class for table "regions".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property integer $used
 *
 * @property Ads[] $ads
 */
class Regions extends \yii\db\ActiveRecord
{
    public $slug_id;
    public $slug_value;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['used'], 'integer'],
            [['slug', 'name'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'name' => Yii::t('app', 'Name'),
            'used' => Yii::t('app', 'Used'),
        ];
    }


    /**
     * @return mixed
     * Получаем готовую для работы прокси
     */
    public static function getRegion()
    {
        $region = Regions::find()->where(['used' => 0])->andWhere(['>', 'id', 0])->one();

        if ($region != null) {
            Regions::setUsed($region);
        } else {
            // обнуляем все прокси
            Regions::setUsed($region);

            $region = Regions::getRegion();
            Regions::setUsed($region);
        }

        return $region;
    }


    /**
     * @param $proxy
     * Ставим флажок "Прокси использован"
     */
    private static function setUsed($region)
    {
        if ($region != null) {
            $region->used = 1;
            $region->update();
        } else {
            Regions::updateAll(['used' => 0]);
        }
    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     * Получаем список регионов
     */
    public static function getRegions()
    {
        $key = 'getRegionsList';

        $result = Y::getCache($key);
        if ($result === false) {
            $result = Regions::find()->where(['>', 'id', 0])->orderBy('name ASC')->all();

            Y::setCache($key, $result);
        }

        return $result;
    }


    public static function getRegionsForSearch()
    {
        $regions = Regions::getRegions();
        $url = Y::parseUrl();

        $list = '';
        foreach ($regions as $region) {

            $selected = '';
            if ($region->id == $url['region_id']) $selected = 'selected';

            $list .= '<option ' . $selected . ' value="' . $region->id . '">' . $region->name . '</option>';
        }

        return $list;
    }

    public static function getRegionById($region_id)
    {
        $region_id = intval($region_id);

        $key = 'getRegionById_' . $region_id;

        $result = Y::getCache($key);
        if ($result === false) {
            $result = Regions::find()->where('id = :id', [':id' => $region_id])->one();

            Y::setCache($key, $result);
        }

        return $result;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['region_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(Cities::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearches()
    {
        return $this->hasMany(Search::className(), ['city_id' => 'id']);
    }
}
