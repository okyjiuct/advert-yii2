<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "metro".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 *
 * @property Ads[] $ads
 */
class Metro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'unique',
                'targetClass' => Metro::className(),
                'message' => 'Такая станция метро уже существует.'],
            [['slug', 'name'], 'string', 'max' => 128]
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'app\components\SlugBehavior',
                'in_attribute' => 'name',
                'out_attribute' => 'slug'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'name' => Yii::t('app', 'Name'),
        ];
    }


    /**
     * @param $category_name
     * @return int
     * Получаем id города, если его нет - добавляем
     */
    public static function getCity($metro_name)
    {
        $metro = Metro::find()
            ->where(['name' => $metro_name])
            ->one();

        if ($metro) {
            return $metro->id;
        }

        $new_metro = new Metro;
        $new_metro->name = $metro_name;
        if ($new_metro->save()) {
            return $new_metro->id;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['metro_id' => 'id']);
    }
}
