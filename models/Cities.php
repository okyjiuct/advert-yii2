<?php

namespace app\models;

use Yii;
use app\components\Y;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 *
 * @property Ads[] $ads
 */
class Cities extends \yii\db\ActiveRecord
{
    public $slug_id;
    public $slug_value;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'unique',
                'targetClass' => Cities::className(),
                'message' => 'Такой город уже существует.'],
            [['region_id'], 'integer'],
            [['slug', 'name'], 'string', 'max' => 128]
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'app\components\SlugBehavior',
                'in_attribute' => 'name',
                'out_attribute' => 'slug'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'name' => Yii::t('app', 'Name'),
            'region_id' => Yii::t('app', 'Region ID'),
        ];
    }

    /**
     * @param $category_name
     * @return int
     * Получаем id города, если его нет - добавляем
     */
    public static function getCity($city_name, $region_id)
    {
        $city = Cities::find()
            ->where('name = :name AND region_id = :region_id', [':name' => $city_name, ':region_id' => $region_id])
            ->one();

        if ($city) {
            return $city->id;
        }

        $new_city = new Cities;
        $new_city->name = $city_name;
        $new_city->region_id = $region_id;
        if ($new_city->save()) {
            return $new_city->id;
        }
    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     * Получаем список регионов
     */
    public static function getCities()
    {
        $key = 'getCitiesList';

        $result = Y::getCache($key);
        if ($result === false) {
            $result = Cities::find()->orderBy('name ASC')->all();

            Y::setCache($key, $result);
        }

        return $result;
    }

    /**
     * @param $region_id
     * @return array|mixed|\yii\db\ActiveRecord[]
     * Получаем список городов из текущего региона
     */
    public static function getCitiesByRegion($region_id)
    {
        $key = 'getCitiesByRegion_' . $region_id;

        $result = Y::getCache($key);

        if ($result === false) {
            if ($region_id > 0) {

                $result = Cities::find()->where('region_id = :region_id AND id > 0', [':region_id' => $region_id])->with(['region'])->orderBy('name ASC')->all();
            } else {
                $result = '';
            }

            Y::setCache($key, $result, 3600);
        }

        return $result;
    }


    public static function getCitiesForSearch()
    {
        $url = Y::parseUrl();

        if ($url['region_id'] > 0) {
            $cities = Cities::getCitiesByRegion($url['region_id']);
        } else {
            return '';
        }

        $list = '';
        foreach ($cities as $city) {

            $selected = '';
            if ($city->id == $url['city_id']) $selected = 'selected';

            $list .= '<option ' . $selected . ' value="' . $city->id . '">' . $city->name . '</option>';
        }

        return $list;
    }


    public static function getCityById($city_id)
    {
        $city_id = intval($city_id);

        $key = 'getCityById_' . $city_id;

        $result = Y::getCache($key);
        if ($result === false) {
            $result = Cities::find()->where('id = :id', [':id' => $city_id])->with(['region'])->one();

            Y::setCache($key, $result);
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }


}
