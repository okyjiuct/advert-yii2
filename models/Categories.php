<?php

namespace app\models;

use Yii;
use app\components\Y;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 *
 * @property Ads[] $ads
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'unique',
                'targetClass' => Categories::className(),
                'message' => 'Такая категория уже существует.'],
            [['slug', 'name'], 'string', 'max' => 128]
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'app\components\SlugBehavior',
                'in_attribute' => 'name',
                'out_attribute' => 'slug'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['category_id' => 'id']);
    }


    public static function getCategories()
    {
        $categories = Categories::find()->where(['>', 'id', 0])->orderBy('name ASC')->all();

        $category_id = Y::getParams('category_id');

        $list = '';
        foreach ($categories as $category) {
            $selected = '';
            if ($category->id == $category_id) $selected = 'selected';

            $list .= '<option ' . $selected . ' value="' . $category->id . '">' . $category->name . '</option>';
        }

        return $list;
    }


    /**
     * @param $category_name
     * @return int
     * Получаем id категории, если ее нет - добавляем
     */
    public static function getCategory($category_name)
    {
        $category = Categories::find()
            ->where('name = :name', ['name' => $category_name])
            ->one();

        if ($category) {
            return $category->id;
        }

        $new_category = new Categories;
        $new_category->name = $category_name;
        if ($new_category->save()) {
            return $new_category->id;
        }
    }
}
