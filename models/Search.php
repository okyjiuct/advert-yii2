<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\Y;

/**
 * This is the model class for table "search".
 *
 * @property integer $id
 * @property string $search
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $category_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Regions $city
 * @property Regions $region
 */
class Search extends \yii\db\ActiveRecord
{
    public $query_list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'required', 'on' => 'active'],
            [['query_list'], 'required', 'on' => 'list'],
            [['region_id', 'city_id', 'category_id', 'updated_at', 'auto_query'], 'integer'],
            [['query'], 'string', 'max' => 256],
            [['ip'], 'string', 'max' => 15]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'query' => Yii::t('app', 'Query'),
            'query_list' => Yii::t('app', 'Query List'),
            'region_id' => Yii::t('app', 'Region ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'active' => Yii::t('app', 'Active'),
            'auto_query' => Yii::t('app', 'Auto Query'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }


    public static function addQuery($attributes, $auto_query = 0)
    {
        $attributes['query'] = trim($attributes['query']);

        $countWords = explode(" ", $attributes['query']);

        if (sizeof($countWords) > 1 && trim($attributes['query']) != '' && trim($attributes['query']) != '0') {

            $search = Search::find()
                ->where('query = :query', [':query' => $attributes['query']])
                ->one();

            if (!$search) {

                $search = new Search;
                $search->attributes = $attributes;
                $search->active = 1;
                $search->auto_query = $auto_query;
                $search->ip = $auto_query ? '' : Yii::$app->getRequest()->getUserIP();
                $search->save();
            }
        }
    }


    public static function addList($list)
    {
        $query_list = explode("\n", $list['Search']['query_list']);

        $i = 0;
        foreach ($query_list as $item) {
            $search = new Search;
            $search->query = $item;
            $search->active = 1;

            if ($search->save()) $i++;
        }

        return $i;
    }


    public static function getRandomQuery()
    {
        $url = Y::parseUrl();

        $key = 'rand_query_' . md5(serialize($url));

        $data = Y::getCache($key);
        if ($data === false) {
            $data = Search::find()
                ->where(['active' => 1])
                ->limit(5)
                ->orderBy('rand()')
                ->all();
            Y::setCache($key, $data);
        }

        return $data;
    }


    public static function countSearch()
    {
        $key = 'countSearch';

        $data = Y::getCache($key);
        if ($data === false) {
            $data = Search::find()->count();
            Y::setCache($key, $data, 600);
        }

        return $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Regions::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }


    /**
     * @param $insert
     * @param $changedAttributes
     * Очищаем кеш после каждого изменения
     */
    public function afterSave($insert, $changedAttributes)
    {
        Y::flushCache();
    }


    /**
     * Очищаем кеш после удаления запросов
     */
    public function afterDelete()
    {
        Y::flushCache();
    }
}
