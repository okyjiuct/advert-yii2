<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ads".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $link
 * @property string $image
 * @property string $price
 * @property integer $category_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $metro_id
 * @property string $author
 * @property string $hash
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Categories $category
 * @property Cities $city
 * @property Metro $metro
 * @property Regions $region
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'link', 'hash', 'slug'], 'required'],
            ['slug', 'unique',
                'targetClass' => Ads::className(),
                'message' => 'Такой slug уже существует.'],
            ['hash', 'unique',
                'targetClass' => Ads::className(),
                'message' => 'Такой hash уже существует.'],
            [['text'], 'string'],
            [['category_id', 'region_id', 'city_id', 'metro_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'link', 'image', 'price', 'author'], 'string', 'max' => 512],
            [['hash'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'text' => Yii::t('app', 'Text'),
            'link' => Yii::t('app', 'Link'),
            'image' => Yii::t('app', 'Image'),
            'price' => Yii::t('app', 'Price'),
            'category_id' => Yii::t('app', 'Category ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'metro_id' => Yii::t('app', 'Metro ID'),
            'author' => Yii::t('app', 'Author'),
            'hash' => Yii::t('app', 'Hash'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Поведения
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class' => 'app\components\SlugBehavior',
                'in_attribute' => 'title',
                'out_attribute' => 'slug'
            ]
        ];
    }


    public static function findByHash()
    {

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetro()
    {
        return $this->hasOne(Metro::className(), ['id' => 'metro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }
}
