<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 09.08.2015
 * Time: 19:26
 */

namespace app\components;

use Yii;

use dosamigos\transliterator\TransliteratorHelper;
use \serhatozles\simplehtmldom\SimpleHTMLDom;
use yii\helpers\Url;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use app\models\Proxy;
use app\models\Settings;

class Y
{
    /**
     * Ярлык для функции dump класса VarDumper для отладки приложения
     * @param mixed $data Переменная для вывода
     * @param depth - глубина дампа переменной
     * @param highlight - подсветка синтаксиса
     */
    public static function dump($data, $end = false, $depth = 10, $highlight = true)
    {
        VarDumper::dump($data, $depth, $highlight);

        if ($end)
            Yii::$app->end();

    }

    /**
     * Получаем текущий контрлллер
     * @return mixed
     */
    public static function thisController()
    {
        return Yii::$app->controller->id;
    }


    /**
     * Получаем текущий экшен
     * @return mixed
     */
    public static function thisAction()
    {
        return Yii::$app->controller->action->id;
    }

    /**
     * Парсим страницу и возвращаем Simple HTML DOM объект
     * @param $href
     * @return mixed
     */
    public static function getPage($href, $referer = false)
    {
        $proxy = Proxy::getProxy();

        fopen('cookie.txt', 'w+');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $href);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);

        if (!$referer) {
            curl_setopt($curl, CURLOPT_REFERER, $href);
        } else {
            curl_setopt($curl, CURLOPT_REFERER, $referer);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, Y::userAgent());

        if (Settings::getSettings()->use_proxy) {
            curl_setopt($curl, CURLOPT_PROXY, "{$proxy['ip_port']}");

            if ($proxy['login_pass'])
                curl_setopt($curl, CURLOPT_PROXYUSERPWD, "{$proxy['login_pass']}");
        }

        curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookie.txt'); // сохранять куки в файл
        curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie.txt');
        $str = curl_exec($curl);
        curl_close($curl);

        $dom = SimpleHTMLDom::str_get_html($str);

        $needle = 'ограничен';
        $pos = strripos($dom, $needle);
        if ($pos !== false) {
            Proxy::setError($proxy);
        }

        return $dom;
    }


    /**
     * Получаем хеш
     *
     * @param $data
     * @return string
     */
    public static function getHash($data = null)
    {
        if ($data) return md5(strrev(md5($data)));

        return md5(strrev(md5(time() . rand(100, 9999999))));
    }


    public static function userAgent()
    {
        $userAgents = array(
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2224.3 Safari/537.36',
            'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 4.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36',
            'Mozilla/5.0 (X11; OpenBSD i386) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2117.157 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1866.237 Safari/537.36',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/4E423F',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36 Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
            'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
            'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20130401 Firefox/31.0',
            'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20120101 Firefox/29.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/29.0',
            'Mozilla/5.0 (X11; OpenBSD amd64; rv:28.0) Gecko/20100101 Firefox/28.0',
            'Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:27.3) Gecko/20130101 Firefox/27.3',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:27.0) Gecko/20121011 Firefox/27.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0',
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20130328 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20130405 Firefox/22.0',
            'Mozilla/5.0 (Microsoft Windows NT 6.2.9200.0); rv:22.0) Gecko/20130405 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:21.0.0) Gecko/20121011 Firefox/21.0.0',
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0',
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (X11; Linux i686; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20130514 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.2; rv:21.0) Gecko/20130326 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20130401 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20130331 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20130330 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130401 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130328 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20130401 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20130331 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (Windows NT 5.0; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
        );

        $rand = array_rand($userAgents);

        return $userAgents[$rand];
    }


    /**
     * @param $key
     * @param $data
     * @param int $duration
     * Устанавливаем кеш
     */
    public static function setCache($key, $data, $duration = 86400)
    {
        $key = $key . md5(dirname(__DIR__));
        $cache = Yii::$app->cache;
        $cache->set($key, $data, $duration);
    }


    /**
     * @param $key
     * @return mixed
     * Читаем кеш
     */
    public static function getCache($key)
    {
        $key = $key . md5(dirname(__DIR__));

        $cache = Yii::$app->cache;
        $data = $cache->get($key);

        return $data;
    }


    /**
     * @param $key
     * Удаляем кеш по ключу
     */
    public static function deleteCache($key)
    {
        $key = $key . md5(dirname(__DIR__));

        $cache = Yii::$app->cache;
        $cache->delete($key);
    }


    /**
     * Удаляем весь кеш
     */
    public static function flushCache()
    {
        $cache = Yii::$app->cache;
        $cache->flush();
    }


    public static function slug($text)
    {
        return Inflector::slug(TransliteratorHelper::process($text), '-', true);
    }


    /**
     * Обрезаем строку
     *
     * @param $string
     * @param int $length
     * @return string
     */
    public static function cut($string, $length = 400)
    {
        if (mb_strlen($string) < $length)
            return $string;
        $string = strip_tags($string);
        $string = mb_substr($string, 0, $length);
        $string = rtrim($string, "!,.-");
        $string = mb_substr($string, 0, strrpos($string, ' '));

        return $string . "...";
    }


    /**
     * @param $num
     * @param $p1
     * @param $p2
     * @param $p5
     * @return mixed
     * Падежи времен
     */
    public static function padezh($num, $p1, $p2, $p5)
    {
        $x = $num % 100;
        $y = ($x % 10) - 1;
        $res = ($x / 10) >> 0 == 1 ? $p5 : ($y & 12 ? $p5 : ($y & 3 ? $p2 : $p1));

        return $res;
    }

    /**
     * @param $sd
     * @param null $snow
     * @return bool|string
     * Фукнкция по работе со временем. "5 минут назад"
     */
    public static function dateDiff($sd, $snow = null)
    {
        if ($snow === null) $snow = time();
        $seconds = $snow - $sd;
        $aseconds = abs($seconds);
        if ($aseconds < 120) {
            return ("Только что");
        } elseif ($aseconds < 3600) {
            return ($seconds < 0 ? "через " : "") . (round($aseconds / 60) % 60) . " " . Y::padezh(round($aseconds / 60) % 60, "минуту", "минуты", "минут") . ($seconds > 0 ? " назад" : "");
        } elseif ($aseconds < 18000) {
            return ($seconds < 0 ? "через " : "") . (round($aseconds / 3600) % 24) . " " . Y::padezh(round($aseconds / 3600) % 24, "час", "часа", "часов") . ($seconds > 0 ? " назад" : "");
        } elseif (($days = abs(strtotime(date("Y-m-d", $sd)) - strtotime(date("Y-m-d", $snow))) / 3600 / 24 >> 0) < 3) {
            if ($seconds >= 0) {
                switch ($days) {
                    case 0:
                        return "сегодня в " . date("H:i", $sd);
                        break;
                    case 1:
                        return "вчера в " . date("H:i", $sd);
                        break;
                    case 2:
                        return "позавчера в " . date("H:i", $sd);
                        break;
                }
            } else {
                switch ($days) {
                    case 0:
                        return "сегодня в " . date("H:i", $sd);
                        break;
                    case 1:
                        return "завтра в " . date("H:i", $sd);
                        break;
                    case 2:
                        return "послезавтра в " . date("H:i", $sd);
                        break;
                }
            }
        } else {
            return date("d.m.Y в H:i", $sd);
        }
    }


    /**
     * @param $slug
     * @return mixed
     * Конвертируем URL в айди и slug
     */
    public static function convertSlug($slug)
    {
        $tmp = explode("-", $slug);
        if (sizeof($tmp) < 2) return null;

        $result['slug_id'] = $tmp[0];
        unset($tmp[0]);
        $result['slug_value'] = implode("-", $tmp);

        return $result;
    }


    public static function getParams($key, $return = null)
    {
        $get = Yii::$app->request->get();

        return (isset($get[$key])) ? $get[$key] : $return;
    }


    public static function parseUrl()
    {
        $get = Yii::$app->request->get();

        $array = [
            'region_id' => '0',
            'region' => '0',
            'city_id' => '0',
            'city' => '0',
            'category_id' => 0,
        ];

        foreach ($get as $key => $item) {
            $array[$key] = $item;
        }

        return $array;
    }


    /**
     * @param $array
     * публикуем метатеги
     */
    public static function metaTags($array)
    {
        Yii::$app->view->registerMetaTag($array);
    }


    /**
     * @param $data
     * @return string
     * Шифруем строку
     */
    public static function enCrypt($data)
    {
        return base64_encode($data);
    }

    /**
     * @param $data
     * @return string
     * Дешифруем строку
     */
    public static function deCrypt($data)
    {
        return base64_decode($data);
    }


    public static function domain()
    {
        $url = Url::to('@web', true);

        $url = explode("/", $url);

        return 'http://' . $url[2] . '/';
    }

}