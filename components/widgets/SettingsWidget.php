<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 11.10.2015
 * Time: 19:35
 */

namespace app\components\widgets;

use yii\base\Widget;
use app\models\Settings;

class SettingsWidget extends Widget
{
    public $options;
    public $return = [];

    public function init()
    {
        parent::init();
        $settings = Settings::getSettings();

        if ($settings) {
            switch ($this->options) {
                case 'counters':
                    if ($settings->metrika) {
                        $this->return[] = '<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter' . $settings->metrika . ' = new Ya.Metrika({ id:' . $settings->metrika . ', clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/' . $settings->metrika . '" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->';
                    }

                    if ($settings->li) {
                        $this->return[] = '<!--LiveInternet counter--><script type="text/javascript"><!--document.write("<a href=\'//www.liveinternet.ru/click\' "+"target=_blank><img src=\'//counter.yadro.ru/hit?t26.18;r"+escape(document.referrer)+((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+";"+Math.random()+"\' alt=\'\' title=\'LiveInternet: показано число посетителей за"+" сегодня\' "+"border=\'0\' width=\'0\' height=\'0\'><\/a>")//--></script><!--/LiveInternet-->';
                    }

                    if ($settings->analytics) {
                        $this->return[] = '<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,"script","//www.google-analytics.com/analytics.js","ga");  ga("create", "' . $settings->analytics . '", "auto");  ga("send", "pageview");</script>';
                    }
                    break;

                case 'verifications':
                    if ($settings->google_verification) {
                        $this->return[] = '<meta name="google-site-verification" content="' . $settings->google_verification . '" />';
                    }

                    if ($settings->yandex_verification) {
                        $this->return[] = '<meta name="yandex-verification" content="' . $settings->yandex_verification . '" />';
                    }
                    break;

                case 'top':
                    if ($settings->top) {
                        $this->return[] = $settings->top;
                    }
                    break;

                case 'topMob':
                    if ($settings->topMob) {
                        $this->return[] = $settings->topMob;
                    }
                    break;

                case 'sidebar':
                    if ($settings->sidebar) {
                        $this->return[] = $settings->sidebar;
                    }
                    break;

                case 'bottom':
                    if ($settings->bottom) {
                        $this->return[] = $settings->bottom;
                    }
                    break;

                case 'bottomMob':
                    if ($settings->bottomMob) {
                        $this->return[] = $settings->bottomMob;
                    }
                    break;

                case 'context':
                    if ($settings->context) {
                        $this->return[] = $settings->context;
                    }
                    break;

                case 'clickunder':
                    if ($settings->clickunder) {
                        $this->return[] = $settings->clickunder;
                    }
                    break;

                case 'otherAd':
                    if ($settings->otherAd) {
                        $this->return[] = $settings->otherAd;
                    }
                    break;
            }
        }


    }

    public function run()
    {
        return "\n" . implode("\n", $this->return) . "\n";
    }
}