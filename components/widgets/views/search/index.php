<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 24.10.2015
 * Time: 11:33
 */

use yii\helpers\Html;
use app\models\Cities;
use app\models\Regions;
use app\models\Categories;
use app\components\Y;

$get = Y::parseUrl();

?>

<form action="/search" method="get" role="form" id="search">
    <div class="form-group field_search_search">
        <label class="control-label" for="search-search">Поиск по сайту</label>
        <input type="text" id="search_query" class="form-control" name="query" placeholder="Поисковый запрос"
               value="<?= isset($get['query']) ? $get['query'] == '0' ? '' : $get['query'] : ''; ?>">

    </div>
    <div class="form-group field_search-region_id">

        <select id="search_region_id" class="form-control" name="region_id">
            <option value="0">--- Регион не выбран ---</option>
            <?= Regions::getRegionsForSearch(); ?>

        </select>
    </div>
    <div class="form-group field_search_city_id">
        <select id="search_city_id" class="form-control" name="city_id">
            <option value="0">--- Город не выбран ---</option>
            <?= Cities::getCitiesForSearch(); ?>
        </select>
    </div>
    <div class="form-group field_search_category_id">
        <select id="search_category_id" class="form-control" name="category_id">
            <option value="0">--- Категория не выбрана ---</option>
            <?= Categories::getCategories(); ?>
        </select>
    </div>
    <button type="submit" class="btn btn-success">Найти</button>
</form>