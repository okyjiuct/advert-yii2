<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 01.11.2015
 * Time: 22:12
 */
use yii\helpers\Html;

if (sizeof($list) > 0) {

    echo '<hr /><h3>Также ищут:</h3>';
    echo '<div class="row">';

    foreach ($list as $item) {
        //echo '<h5><a href="/search/' . $item->region_id . '/' . $item->city_id . '/' . $item->category_id . '?query=' . Html::encode($item->query) . '">' . $item->query . '</a></h5>';
        echo '<h5 class="col-xs-6 col-md-12"><a href="/search/0/0/0?query=' . Html::encode($item->query) . '">' . $item->query . '</a></h5>';
    }
    echo '</div>';
}

