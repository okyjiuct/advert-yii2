<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.10.15
 * Time: 17:04
 */

use app\components\Y;
use yii\helpers\Html;

if (sizeof($list) > 0) {
    $regions = '';
    $name = '';
    foreach ($list as $item) {
        $label = '';
        if ($item->id == $region_id) {
            $label = 'label label-warning';
            $name = $item->name;
        }

        $link = Html::a(
            $item->name,
            [
                'site/view',
                'region_id' => $item->id,
                'region' => $item->slug,
                'city_id' => 0,
                'city' => 0
            ],
            ['class' => $label]
        );


        $regions .= '<div class="col-lg-3 col-md-4 col-xs-6">' . $link . '</div>';
    }

    if ($name) {
        echo '<div class="spoiler-btn" > Список регионов (выбран <strong>' . $name . ')</strong> </div>
            <div class="spoiler-body collapse"> ' . $regions . '</div > ';
    } else {
        echo '<div class="spoiler-btn" > Список регионов</strong > </div>
            <div class="spoiler-body collapse"> ' . $regions . '</div> ';
    }
}

