<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.10.15
 * Time: 11:13
 */

use app\components\Y;
use yii\helpers\Html;



if (sizeof($list) > 1) {
    $cities = '';
    $name = '';
    foreach ($list as $item) {
        $label = '';
        if ($item->id == $city_id) {
            $label = 'label label-warning';
            $name = $item->name;
        }

        $link = Html::a(
            $item->name,
            [
                'site/view',
                'region_id' => $item->region->id,
                'region' => $item->region->slug,
                'city_id' => $item->id,
                'city' => $item->slug
            ],
            ['class' => $label]
        );


        $cities .= '<div class="col-lg-3 col-md-4 col-xs-6">' . $link . '</div>';
    }

    if ($name) {
        echo '<div class="spoiler-btn" > Список городов по региону (выбран <strong>' . $name . ')</strong> </div>
            <div class="spoiler-body collapse"> ' . $cities . '</div > ';
    } else {
        echo '<div class="spoiler-btn" > Список городов по региону</strong > </div>
            <div class="spoiler-body collapse"> ' . $cities . '</div> ';
    }
}

