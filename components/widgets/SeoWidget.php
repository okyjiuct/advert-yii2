<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 25.10.2015
 * Time: 18:44
 */

namespace app\components\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\Settings;
use app\models\Cities;
use app\models\Regions;
use app\models\Ads;
use app\components\Y;

class SeoWidget extends Widget
{
    public $options;
    public $render;

    public function init()
    {
        parent::init();
        $settings = Settings::getSettings();

        if ($settings) {
            $url = Y::parseUrl();
            $page = isset($url['page']) ? $url['page'] : 0;

            $numPage = '';
            if ($page > 1) $numPage = '. Страница ' . $page;

            $keywordsContent = 'доска объявлений, покупка, продажа, объявления, авито, avito, недорого, бу, б/у';
            $descriptionContent = 'Объявления о покупке и продаже ' . $settings->title;

            if ($url['city_id'] > 0) { // города
                $city = Cities::getCityById($url['city_id']);
            }

            if ($url['city_id'] == 0 && $url['region_id'] > 0) { // регионы
                $region = Regions::getRegionById($url['region_id']);
            }


            if (isset($url['query'])) { // результаты поиска

                $str = '';
                if (isset($city)) {
                    $str = ' по региону ' . $city->region->name . ' в городе ' . $city->name;
                }

                if (isset($region)) {
                    $str = ' по региону ' . $region->name;
                }

                $descriptionContent = 'Результаты поиска по запросу ' . $url['query'] . $str;

            } elseif ($url['city_id'] > 0) { // города

                $keywordsContent .= ', ' . $city->region->name . ', ' . $city->name . $numPage;
                $descriptionContent .= ' по региону ' . $city->region->name . ', ' . $city->name . $numPage;

            } elseif ($url['region_id'] > 0) { // регионы

                $keywordsContent .= ', ' . $region->name . $numPage;
                $descriptionContent .= ' по региону ' . $region->name . $numPage;

            } else { // главная

            }

            // публикуем keywords
            Y::metaTags(['name' => 'keywords', 'content' => $keywordsContent]);

            // публикуем description
            Y::metaTags(['name' => 'description', 'content' => $descriptionContent]);

            // индексация навигации
            if (!$settings->index_navigation && $page > 1) {
                Y::metaTags(['name' => 'robots', 'content' => 'noindex, nofollow']);
            }
        }


    }

    public function run()
    {
        echo $this->render;
    }
}