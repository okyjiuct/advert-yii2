<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 24.10.2015
 * Time: 11:30
 */

namespace app\components\widgets;

use Yii;
use yii\base\Widget;
use app\components\Y;

use app\models\Cities;
use app\models\Regions;
use app\models\Search;

class SearchWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('search/index');
    }
}