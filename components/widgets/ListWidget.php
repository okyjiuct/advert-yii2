<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.10.15
 * Time: 10:57
 */

namespace app\components\widgets;

use Yii;
use yii\base\Widget;
use app\models\Regions;
use app\models\Cities;
use app\models\Search;
use app\components\Y;

class ListWidget extends Widget
{
    public $options;

    public $region;
    public $city;
    public $ad;

    public $list;
    public $view = 'list/cities';

    public $array = ['slug_id' => '', 'slug_value' => ''];

    public function init()
    {
        parent::init();

        $url = Y::parseUrl();

        $this->region = $url['region_id'];
        $this->city = $url['city_id'];

        switch ($this->options) {
            case 'regions':
                $this->list = Regions::getRegions();
                $this->view = 'list/regions';
                break;

            case 'cities':
                $this->list = Cities::getCitiesByRegion($this->region);
                $this->view = 'list/cities';
                break;

            case 'randQueries':
                $this->list = Search::getRandomQuery();
                $this->view = 'list/random';
                break;

            case 'relations':
                /** TODO вывод похожих объявлений по данному региону */
                $this->list = '';
                break;

            default:
                $this->list = Regions::getRegions();
                $this->view = 'list/regions';
                break;
        }
    }

    public function run()
    {
        return $this->render($this->view, [
            'list' => $this->list,
            'region_id' => $this->region,
            'city_id' => $this->city,
        ]);
    }
}