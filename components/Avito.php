<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 17.10.2015
 * Time: 16:34
 */

namespace app\components;

use app\models\Settings;
use app\models\Regions;
use app\models\Categories;
use app\models\Cities;
use app\models\Search;
use app\models\Ads;
use app\components\Y;


class Avito
{
    public static function getAds()
    {
        $region = Regions::getRegion();
        $mobHost = 'https://m.avito.ru/';
        $webHost = 'https://www.avito.ru';

        $url = $mobHost . $region->slug . '/' . Settings::getSettings()->slug . '/?user=1';

        $html = Y::getPage($url, $mobHost . $region->slug);

        if ($html !== false) {

            // получили страницу с объявлениями
            $result = $html->find('.b-item');

            $count = 0;
            foreach ($result as $element) {
                // спим перед парсингом
                usleep(mt_rand(5000000, 15000000));

                $data = [
                    'title' => '',
                    'slug' => '',
                    'text' => '',
                    'link' => '',
                    'image' => '',
                    'price' => '',
                    'category_id' => '',
                    'region_id' => '',
                    'city_id' => '',
                    'metro_id' => '',
                    'author' => '',
                    'hash' => '',
                    'shop' => '',
                ];

                $data['region_id'] = $region->id;

                // link
                foreach ($element->find('.item-link') as $el) {
                    $data['link'] = $webHost . trim($el->href);
                    $data['mobLink'] = $mobHost . trim($el->href);

                    $data['hash'] = Y::getHash($data['link']);
                    // если уже парсили - пропускаем

                    $exists = Ads::find()
                        ->where(['hash' => $data['hash']])
                        ->exists();
                }
                if ($exists || $data['link'] == '') continue;


                // shop - фильтруем магазины
                foreach ($element->find('.info-shop') as $el) {
                    $data['shop'] = trim($el->plaintext);
                }
                if ($data['shop'] != '') continue;

                // title
                foreach ($element->find('.header-text') as $el) {
                    $data['title'] = trim($el->plaintext);
                }
                if ($data['title'] == '') continue;

                // каждый n-ый запрос сохраняем как введенный юзером
                // защита от дурака - если чаще, чем каждый 25 запрос - делаем каждый 25
                $itemRand = (int)Settings::getSettings()->query_to_search < 25 ? 25 : Settings::getSettings()->query_to_search;

                $rand = mt_rand(1, $itemRand);
                if ($rand == 1 && mb_strlen($data['title']) > 15) {
                    // добавляем новый запрос в базу
                    $attributes['query'] = $data['title'];
                    Search::addQuery($attributes, 1);
                    continue;
                }

                // price
                foreach ($element->find('.item-price') as $el) {
                    $price = trim($el->plaintext);
                    $data['price'] = trim(str_replace("&nbsp;", " ", $price));
                }
                if ($data['price'] == '') continue;

                // city
                if ($region->slug == 'moskva' || $region->slug == 'sankt-peterburg') {
                    $data['city'] = $region->name;
                    foreach ($element->find('.info-metro-district') as $el) {
                        $metro = trim($el->plaintext);
                        $data['metro'] = trim(str_replace("&nbsp;", " ", $metro));
                        $data['metro_id'] = Cities::getCity($data['metro']);

                        unset($data['metro']);
                    }
                } else {
                    foreach ($element->find('.info-location') as $el) {
                        $data['metro_id'] = '';
                        $city = trim($el->plaintext);
                        $data['city'] = trim(str_replace("&nbsp;", " ", $city));
                        $data['city_id'] = Cities::getCity($data['city'], $region->id);

                        unset($data['city']);
                        unset($data['metro']);
                    }
                }
                if ($data['city_id'] == '') continue;

                // image
                foreach ($element->find('.pseudo-img') as $el) {
                    $image = trim($el->style);
                    preg_match("/\((.*)\)/i", $image, $found);
                    if (isset($found[1])) {
                        $data['image'] = $found[1];
                    }
                }
                if ($data['image'] == '') continue;

                // парсим полное объявление
                $adHtml = Y::getPage($data['mobLink'], $url);

                if ($adHtml === false) continue;

                //category
                $data['category_name'] = [];
                foreach ($adHtml->find('.info-params .param') as $el) {
                    $data['category_name'][] = trim($el->plaintext);
                }
                $data['category_name'] = array_reverse($data['category_name']);

                if (isset($data['category_name'][0])) {
                    $data['category_id'] = Categories::getCategory($data['category_name'][0]);

                    unset($data['category_name']);
                } else continue;

                // text
                foreach ($adHtml->find('.description-preview-wrapper') as $el) {
                    $data['text'] = trim($el->plaintext);
                }

                // author
                foreach ($adHtml->find('.person-name') as $el) {
                    $data['author'] = trim($el->plaintext);
                }

                $ad = new Ads;
                $ad->attributes = $data;

                if ($ad->save()) $count++;
            }

            if (Settings::getSettings()->delete_old_ads) {
                $date = time() - 30 * 24 * 60 * 60;
                Ads::deleteAll(['<', 'created_at', $date]);
            }

            echo "Add $count ads\r\n";

            unset($ad);
            unset($data);
            unset($result);
        } else echo "Parse error\r\n";

        unset($html);
    }
}