<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 11.10.2015
 * Time: 11:05
 */

namespace app\commands;

use yii\console\Controller;
use app\components\Avito;

class ParserController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        Avito::getAds();
    }
}