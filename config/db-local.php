<?php
/**
 * Created by PhpStorm.
 * User: kohone
 * Date: 08.11.2015
 * Time: 12:38
 */

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=db_name',
    'username' => 'db_user',
    'password' => 'db_pass',
    'charset' => 'utf8',
    'queryCacheDuration' => 3600
];
