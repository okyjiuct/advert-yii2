<?php

namespace app\controllers;

use Yii;

use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;

use app\models\LoginForm;
use app\models\RegForm;
use app\models\Cities;
use app\models\Regions;
use app\models\Search;
use app\models\Settings;
use app\models\Users;
use app\models\Ads;
use app\components\Y;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'reg'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'reg'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ads::find()->with(['city', 'category', 'region']),
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ]
        ]);

        $settings = Settings::getSettings();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' => 'Объявления о покупке и продаже ' . $settings->title,
        ]);
    }

    public function actionView($region_id = 0, $region = 0, $city_id = 0, $city = 0)
    {
        // подгружаем города по региону в поиск
        $request = Yii::$app->request;
        if ($request->isAjax) {

            $get = Yii::$app->request->get();

            if ($get['id'] > 0) {
                $return = '<select id="search_city_id" class="form-control" name="city_id">
                <option id="city" value="0">--- Город не выбран ---</option>';

                $cities = Cities::getCitiesByRegion($get['id']);

                foreach ($cities as $city) {
                    $return .= '<option value="' . $city->id . '">' . $city->name . '</option>';
                }

                $return .= '</select>';
            } else {
                $return = '<select id="search_city_id" class="form-control" name="city_id">
                <option id="city" value="0">--- Город не выбран ---</option>
            </select>';
            }

            return $this->renderAjax('_ajax', [
                'return' => $return,
            ]);
        }

        $url = Y::parseUrl();
        $settings = Settings::getSettings();

        if ($url['city_id'] > 0) { // city view

            $city = Cities::getCityById($url['city_id']);

            $dataProvider = new ActiveDataProvider([
                'query' => Ads::find()->where(['city_id' => $url['city_id']])->with(['city', 'category', 'region']),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC,
                    ]
                ]
            ]);

            $page = isset($url['page']) ? $url['page'] : 0;
            $numPage = '';
            if ($page > 1) $numPage = '. Страница ' . $page;

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'title' => 'Объявления о покупке и продаже ' . $settings->title . ' по региону ' . $city->region->name . ', ' . $city->name . $numPage,
            ]);


        } else if ($url['region_id'] > 0) { // region view

            $region = Regions::getRegionById($url['region_id']);

            $dataProvider = new ActiveDataProvider([
                'query' => Ads::find()->where(['region_id' => $url['region_id']])->with(['city', 'category', 'region']),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC,
                    ]
                ]
            ]);

            $page = isset($url['page']) ? $url['page'] : 0;
            $numPage = '';
            if ($page > 1) $numPage = '. Страница ' . $page;

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'title' => 'Объявления о покупке и продаже ' . $settings->title . ' по региону ' . $region->name . $numPage,
            ]);
        }
    }

    public function actionSearch()
    {
        $url = Y::parseUrl();

        $region = '';
        $city = '';
        $category = '';
        $regionParams = '';
        $cityParams = '';
        $categoryParams = '';

        $cityToTitle = false;
        $regionToTitle = false;


        if ($url['region_id'] > 0) {
            $region = 'region_id = :region_id';
            $regionParams = [':region_id' => intval($url['region_id'])];

            $regionToTitle = Regions::getRegionById($url['region_id']);
        }
        if ($url['city_id'] > 0) {
            $city = 'city_id = :city_id';
            $cityParams = [':city_id' => intval($url['city_id'])];

            $cityToTitle = Cities::getCityById($url['city_id']);
        }
        if ($url['category_id'] > 0) {
            $category = 'category_id = :category_id';
            $categoryParams = [':category_id' => intval($url['category_id'])];
        }

        $attributes = [
            'query' => $url['query'],
            'region_id' => $url['region_id'],
            'city_id' => $url['city_id'],
            'category_id' => $url['category_id'],
        ];

        Search::addQuery($attributes);

        if (trim($url['query'])) {
            $dataProvider = new ActiveDataProvider([
                'query' => Ads::find()
                    ->where('MATCH (title,text) AGAINST (:query)', [':query' => $url['query']])
                    ->andWhere($region)
                    ->andWhere($city)
                    ->andWhere($category)
                    ->with(['city', 'category', 'region'])
                    ->addParams($regionParams)
                    ->addParams($cityParams)
                    ->addParams($categoryParams),
                'pagination' => [
                    'pageSize' => 25,
                ],
                'totalCount' => 25,
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Ads::find()
                    ->where($region)
                    ->andWhere($city)
                    ->andWhere($category)
                    ->with(['city', 'category', 'region'])
                    ->addParams($regionParams)
                    ->addParams($cityParams)
                    ->addParams($categoryParams),
                'pagination' => [
                    'pageSize' => 25,
                ],
                'totalCount' => 25,
            ]);
        }

        if ($dataProvider->getCount()) {

            if ((isset($url['page']) && $url['page'] == 1) || (isset($url['page']) && $url['page'] > 1)) {
                return $this->redirect(
                    [
                        'site/search',
                        'query' => $url['query'],
                        'region_id' => $url['region_id'],
                        'city_id' => $url['city_id'],
                        'category_id' => $url['category_id'],
                    ],
                    301
                );
            }
        }

        $str = '';
        if ($cityToTitle) {
            $str = ' по региону ' . $cityToTitle->region->name . ' в городе ' . $cityToTitle->name;
        } elseif ($regionToTitle) {
            $str = ' по региону ' . $regionToTitle->name;
        }

        $title = $url['query'] . $str;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' => $title,
        ]);
    }


    /**
     * @param int $number
     * @return mixed
     * редирект на сайт оригинального объявления
     */
    public function actionTo($number = 0)
    {
        $result = Y::deCrypt($number);

        list($id) = explode("###", $result);

        $model = Ads::find()->where('id = :id', [':id' => $id])->one();

        $link = $model->link;

        if ($link == null) {
            return $this->goHome();
        } else {
            $this->layout = 'redirect';

            Y::metaTags(['http-equiv' => 'refresh', 'content' => '1;' . $link]);

            return $this->render('redirect');
        }
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                // form inputs are valid, do something here
                return $this->goBack();
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionReg()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            // если уже есть зарегистрированные пользователи - новые регистрации отключаем
            $user = Users::find()->where(['>', 'id', 0])->one();

            if ($user) {
                return $this->goHome();
            }
        }

        $model = new RegForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($user = $model->reg()) {
                    if (Yii::$app->getUser()->login($user)) {
                        return $this->goHome();
                    }
                } else {
                    Yii::$app->session->setFlash('danger', 'Возникла ошибка при регистрации.');
                    Yii::error('Ошибка при регистрации');

                    return $this->refresh();
                }
            }
        }

        return $this->render('reg', [
            'model' => $model,
        ]);
    }

    public function actionQueries()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Search::find(),
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ]
        ]);


        return $this->render('queries', [
            'dataProvider' => $dataProvider,
            'title' => 'Полный список поисковых запросов',
        ]);
    }
}
